/**
 * Copyright Digital Engagement Xperience 2015
 * Created by  shawn on 15-03-13.
 * @description
 */
/*jslint node: true */
"use strict";


/**
 * For testing purposes
 * @param args
 * @returns {{location: string, time: string}}
 */

 var here = "500 -500";
 var now = "123456789";

dexit.msdk.extract = function(args) {
    navigator.geolocation.getCurrentPosition(onSuccess, onError);
    alert(here + " " + now);
    return { "location":here, "time":now}
};

// onSuccess Geolocation
//
function onSuccess(position) {
    here = position.coords.latitude + " " + position.coords.longitude;
    now = position.timestamp;
}

// onError Callback receives a PositionError object
//
function onError(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}
